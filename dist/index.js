'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _accountStorage = require('./account-storage');

var _accountStorage2 = _interopRequireDefault(_accountStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _accountStorage2.default;
module.exports = exports['default'];