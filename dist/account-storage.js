"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _ambisafeClientJavascript = require("ambisafe-client-javascript");

var _ambisafeClientJavascript2 = _interopRequireDefault(_ambisafeClientJavascript);

var _ethereumjsUtil = require("ethereumjs-util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AccountStorage = function () {
    function AccountStorage(contractAddress, web3, accountSaverAddress) {
        _classCallCheck(this, AccountStorage);

        this.contractAddress = contractAddress;
        this._web3 = web3;
        this.password = '';
        this.accountSaverAddress = accountSaverAddress;
    }

    _createClass(AccountStorage, [{
        key: "addAccount",
        value: function addAccount(address, data, callback) {
            this.contract.addAccount(address, JSON.stringify(data), { from: this.accountSaverAddress }, callback);
        }
    }, {
        key: "getAccountByAddress",
        value: function getAccountByAddress(address, callback) {
            var _this = this;

            this.contract.getSerialized.call(address, function (err, data) {
                if (err) {
                    callback(err);
                } else {
                    if (!data) {
                        callback('No account for address: ' + address);
                    } else {
                        try {
                            callback(null, new _ambisafeClientJavascript2.default.Account(JSON.parse(data), _this.password));
                        } catch (e) {
                            callback('Wrong password for address: ' + address);
                        }
                    }
                }
            });
        }
    }, {
        key: "getPrivateKey",
        value: function getPrivateKey(address, callback) {
            this.getAccountByAddress(address, function (err, account) {
                if (err) {
                    callback(err);
                } else {
                    callback(null, new Buffer(account.get('private_key'), "hex"));
                }
            });
        }
    }, {
        key: "web3",
        get: function get() {
            return this._web3;
        },
        set: function set(web3) {
            this._web3 = web3;
            this._contract = null;
        }
    }, {
        key: "contract",
        get: function get() {
            if (!this._contract) {
                this._contract = this.web3.eth.contract(AccountStorage.abi).at(this.contractAddress);
            }
            return this._contract;
        }
    }], [{
        key: "abi",
        get: function get() {
            return [{ "constant": false, "inputs": [{ "name": "_new", "type": "address" }], "name": "setOwner", "outputs": [{ "name": "", "type": "bool" }], "type": "function" }, { "constant": true, "inputs": [{ "name": "_address", "type": "address" }], "name": "getSerialized", "outputs": [{ "name": "", "type": "string" }], "type": "function" }, { "constant": false, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }], "name": "setRecovered", "outputs": [{ "name": "", "type": "bool" }], "type": "function" }, { "constant": false, "inputs": [{ "name": "_addr", "type": "address" }, { "name": "_serialized", "type": "string" }], "name": "addAccount", "outputs": [{ "name": "", "type": "bool" }], "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "uint256" }], "name": "accounts", "outputs": [{ "name": "addr", "type": "address" }, { "name": "serialized", "type": "string" }, { "name": "recoveredTo", "type": "uint256" }], "type": "function" }, { "inputs": [], "type": "constructor" }];
        }
    }]);

    return AccountStorage;
}();

exports.default = AccountStorage;
module.exports = exports["default"];