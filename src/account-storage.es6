import Ambisafe from "ambisafe-client-javascript";
import {privateToAddress} from "ethereumjs-util";

export default class AccountStorage {
    constructor(contractAddress, web3, accountSaverAddress) {
        this.contractAddress = contractAddress;
        this._web3 = web3;
        this.password = '';
        this.accountSaverAddress = accountSaverAddress;
    }

    get web3() {
        return this._web3;
    }

    set web3(web3) {
        this._web3 = web3;
        this._contract = null;
    }

    static get abi() {
        return [{"constant":false,"inputs":[{"name":"_new","type":"address"}],"name":"setOwner","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":true,"inputs":[{"name":"_address","type":"address"}],"name":"getSerialized","outputs":[{"name":"","type":"string"}],"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"}],"name":"setRecovered","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":false,"inputs":[{"name":"_addr","type":"address"},{"name":"_serialized","type":"string"}],"name":"addAccount","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"accounts","outputs":[{"name":"addr","type":"address"},{"name":"serialized","type":"string"},{"name":"recoveredTo","type":"uint256"}],"type":"function"},{"inputs":[],"type":"constructor"}];
    }

    get contract() {
        if (!this._contract) {
            this._contract = this.web3.eth.contract(AccountStorage.abi).at(this.contractAddress);
        }
        return this._contract;
    }

    addAccount(address, data, callback) {
        this.contract.addAccount(address, JSON.stringify(data), {from: this.accountSaverAddress}, callback);
    }

    getAccountByAddress(address, callback) {
        this.contract.getSerialized.call(address, (err, data) => {
            if (err) {
                callback(err);
            } else {
                if (!data) {
                    callback('No account for address: ' + address);
                } else {
                    try {
                        callback(null, new Ambisafe.Account(JSON.parse(data), this.password));
                    } catch (e) {
                        callback('Wrong password for address: ' + address);
                    }
                }
            }
        });
    }

    getPrivateKey(address, callback) {
        this.getAccountByAddress(address, (err, account) => {
            if (err) {
                callback(err);
            } else {
                callback(null, new Buffer(account.get('private_key'), "hex"));
            }
        });
    }
}
