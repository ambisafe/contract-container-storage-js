import AccountManager from '../src/account-storage';
import Ambisafe from 'ambisafe-client-javascript';
import Web3 from 'web3';
import ProviderEngine from 'web3-provider-engine';
import HookedWalletEthTxSubprovider from 'web3-provider-engine/subproviders/hooked-wallet-ethtx';
import NonceTrackerSubprovider from 'web3-provider-engine/subproviders/nonce-tracker';
import Web3Subprovider from 'web3-provider-engine/subproviders/web3';
import ethereumUtil from 'ethereumjs-util';
import assert from 'assert';

describe('AccountManager', function () {
    let engine = new ProviderEngine();
    let web3 = new Web3(engine);
    let pk = '';
    let manager = new AccountManager('0x152c21d6944f32c6b45605af12bb9b7231a456e7', web3, pk);
    engine.addProvider(new NonceTrackerSubprovider());
    engine.addProvider(new HookedWalletEthTxSubprovider(manager));
    engine.addProvider(new Web3Subprovider(new web3.providers.HttpProvider('http://127.0.0.1:8545')));
    engine.start();

    it('should create and get Account', function (done) {
        this.timeout(300000);
        // manager.askPassword((err, password) => {
            let password = 'password';
            let account = Ambisafe.generateAccount('ETH', password);
            let address = '0x' + ethereumUtil.privateToAddress(
                    '0x' + account.get('private_key')
                ).toString('hex');
                var txId;
                manager.addAccount(address, account.getContainer(), (err, result) => {
                    console.log(err);
                    console.log(result);
                    txId = result;
                });

                engine.on('block', (block) => {
                    web3.eth.getTransactionReceipt(txId, function (err, result) {
                        if (result) {
                            manager.getAccountByAddress(address, (err, account) => {
                                console.log(err);
                                console.log(account);
                                assert.equal(typeof account, 'object');
                                done();
                            });
                        }
                    });
                });
        // });
        
    });

    it('should send transaction', function () {

    });
});