var path = require('path')

module.exports = {
  entry: "./src/index.es6",
  output: {
    path: path.join(__dirname, 'browser'),
    filename: 'bundle.js'
  },

  resolve: {
    extensions: ['', '.js', 'index.js', '.json', 'index.json']
  },

  module: {
    preLoaders: [
        { test: /\.json$/, loader: 'json'},
    ],
    loaders: [
        { test: /\.es6$/, exclude: /node_modules/, loader: 'babel'},
    ]
  },
  resolveLoader: {
        root: path.join(__dirname, 'node_modules')
  },
  devtool: '#eval'
}